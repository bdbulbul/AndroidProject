package com.example.bulbul.weatherapiappsdemo;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button dhakaCity,wasingtonCity,riadCity,sydnyCity,myCity;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);


        dhakaCity= (Button) findViewById(R.id.button);
        wasingtonCity= (Button) findViewById(R.id.button2);
        riadCity= (Button) findViewById(R.id.button3);
        sydnyCity= (Button) findViewById(R.id.button4);
        myCity= (Button) findViewById(R.id.button5);

        dhakaCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dhaksCity=new Intent(MainActivity.this,Dhaka.class);
                dhaksCity.putExtra("name","dhaka");
                startActivity(dhaksCity);
            }
        });

        wasingtonCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wasingtonCity=new Intent(MainActivity.this,Dhaka.class);
                wasingtonCity.putExtra("name","wasington");
                startActivity(wasingtonCity);
            }
        });

        riadCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent riadCity=new Intent(MainActivity.this,Dhaka.class);
                riadCity.putExtra("name","riad");
                startActivity(riadCity);
            }
        });

        sydnyCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sydnyCity=new Intent(MainActivity.this,Dhaka.class);
                sydnyCity.putExtra("name","sydny");
                startActivity(sydnyCity);
            }
        });

        myCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myCity=new Intent(MainActivity.this,Dhaka.class);
                myCity.putExtra("name","mycity");
                startActivity(myCity);
            }
        });





    }


}