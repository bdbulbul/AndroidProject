package com.example.bulbul.sppinerdemoproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String []  countryNames;
    private Button button;
    private TextView textView;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryNames=getResources().getStringArray(R.array.country_names);

        button= (Button) findViewById(R.id.button);
        textView= (TextView) findViewById(R.id.textview);
        spinner= (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,R.layout.sample_layout,R.id.sampleViewId,countryNames);
        spinner.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String values=spinner.getSelectedItem().toString();
                textView.setText(values);
            }
        });


    }
}
