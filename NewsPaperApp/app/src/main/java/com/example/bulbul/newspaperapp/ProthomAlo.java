package com.example.bulbul.newspaperapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ProthomAlo extends AppCompatActivity {
    private WebView prothomAloWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prothom_alo);

        prothomAloWebView= (WebView) findViewById(R.id.protholAloWebViewId);

        prothomAloWebView.getSettings().getJavaScriptEnabled();
        prothomAloWebView.getSettings().setBuiltInZoomControls(true);
        prothomAloWebView.setWebViewClient(new WebViewClient());
        prothomAloWebView.loadUrl("http://www.prothom-alo.com");
    }

    @Override
    public void onBackPressed() {
        if(prothomAloWebView.canGoBack()){
            prothomAloWebView.goBack();
        }else {
            super.onBackPressed();
        }
    }
}
