package com.example.bulbul.newspaperapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button banglaNewsButton,englishNewsButton,sportsNewsButton,onlineNewsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        banglaNewsButton= (Button) findViewById(R.id.button);
        englishNewsButton= (Button) findViewById(R.id.button2);
        sportsNewsButton= (Button) findViewById(R.id.button3);
        onlineNewsButton= (Button) findViewById(R.id.button4);


        banglaNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent banglaNews=new Intent(MainActivity.this,BanglaNewsPapers.class);
                startActivity(banglaNews);
            }
        });

        englishNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent englishNews=new Intent(MainActivity.this,EnglishNewsPapers.class);
                startActivity(englishNews);
            }
        });



    }


}
