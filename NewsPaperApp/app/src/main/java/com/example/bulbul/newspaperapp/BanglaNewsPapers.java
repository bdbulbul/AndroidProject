package com.example.bulbul.newspaperapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BanglaNewsPapers extends AppCompatActivity {

    private Button prothomAlo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bangla_news_papers);

        prothomAlo= (Button) findViewById(R.id.button6);

        prothomAlo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prothomAlo=new Intent(BanglaNewsPapers.this,ProthomAlo.class);
                startActivity(prothomAlo);
            }
        });
    }
}
