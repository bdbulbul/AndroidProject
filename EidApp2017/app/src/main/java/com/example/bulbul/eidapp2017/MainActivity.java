package com.example.bulbul.eidapp2017;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button eidFitrText,eidAdhaText,startEid,salatEid,activityBeforeSalat,rulesofKurbani,afterKurbani,eidsms,eidGallery;
    private AlertDialog.Builder alertDialogBuilder;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Find All Id and Storage in variable

        eidFitrText= (Button) findViewById(R.id.EidulFitrButton);
        eidAdhaText= (Button) findViewById(R.id.EidulAdhaButton);
        startEid= (Button) findViewById(R.id.EidulFitrActivityButton);
        salatEid= (Button) findViewById(R.id.salatButton);
        activityBeforeSalat= (Button) findViewById(R.id.beforeSalatButton);
        rulesofKurbani= (Button) findViewById(R.id.kurbaniButton);
        afterKurbani= (Button) findViewById(R.id.beforeKurbaniButton);
        eidsms= (Button) findViewById(R.id.eidSmsButton);
        eidGallery= (Button) findViewById(R.id.eidGallaryButton);

        //Button=Eid Ul Fiter



        eidFitrText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.eid_fitr_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //ButtonText = Eid Ul Adha

        eidAdhaText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.eid_adha_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });
        //----------------Start Of Eid Button--------------

        startEid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.eid_ftr_duty_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //Salat Eid Button

        salatEid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.salat_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //activity Before Salat


        activityBeforeSalat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.before_salat_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //Rules of Kurbani

        rulesofKurbani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.kurbanir_niyom_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //After Kurbani

        afterKurbani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.after_kurbani_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //Eid SMS

        eidsms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage(R.string.eid_sms_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //Eid Photo Gallery

        eidGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setIcon(R.drawable.ga);



                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();

            }
        });



    }


}
