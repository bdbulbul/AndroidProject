package com.example.bulbul.sqlitedatabasedemo_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText eTextName,eTextAdress,eTextEMail;
    Button submitBtn,showBtn;
    DatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eTextName= (EditText) findViewById(R.id.etName);
        eTextAdress= (EditText) findViewById(R.id.etAddress);
        eTextEMail= (EditText) findViewById(R.id.etEmail);

        submitBtn= (Button) findViewById(R.id.submitBtn);
        showBtn= (Button) findViewById(R.id.showButton);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Store Info From User

                String userName=eTextName.getText().toString();
                String userAddress=eTextAdress.getText().toString();
                String userEmail=eTextEMail.getText().toString();

                if(userName.equals("")|| userAddress.equals("")|| userEmail.equals("")){
                    Toast.makeText(MainActivity.this,"Please insert Data",Toast.LENGTH_SHORT).show();
                }else{
                    helper = new DatabaseHelper(MainActivity.this);
                    helper.insertIntoDB(userName,userAddress,userEmail);

                    Toast.makeText(MainActivity.this,"Data has stored sucessfully",Toast.LENGTH_SHORT).show();
                }



            }
        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


}
