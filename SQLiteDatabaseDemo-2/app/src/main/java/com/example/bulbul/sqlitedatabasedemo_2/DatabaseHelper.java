package com.example.bulbul.sqlitedatabasedemo_2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by bulbul on 10/22/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="STUDENT_DB";
    private static final int DATABASE_VERSION=1;
    private static final String MEMBER_TABLE="stureg";
    private static final String TAB_NAME="userName";
    private static final String TAB_ADDRESS="userAddress";
    private static final String TAB_EMAIL="userEmail";
    private static final String TABLE_NAME="create table" + MEMBER_TABLE + "("+TAB_NAME+" TEXT,"+TAB_ADDRESS+" TEXT,"+TAB_EMAIL+" TEXT primary key)";

    Context context;
    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        this.context=context;
    }

    public void insertIntoDB(String userName, String userAddress, String userEmail) {
    SQLiteDatabase db =this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TAB_NAME,userName);
        contentValues.put(TAB_ADDRESS,userAddress);
        contentValues.put(TAB_EMAIL,userEmail);

        //Insert data

        db.insert(MEMBER_TABLE,null,contentValues);
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL(TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF NOT EXISTS "+MEMBER_TABLE);
    }
}
