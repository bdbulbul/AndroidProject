package com.example.bulbul.jsonparsingmm;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView= (TextView) findViewById(R.id.textView);


        DownloadTask task=new DownloadTask();
        task.execute("https://newsapi.org/v1/articles?source=al-jazeera-english&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
    }
    public class DownloadTask extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String weatherInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(weatherInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    // Log.i("Main",jsonPart.getString("main"));
                   textView.setText(jsonPart.getString("title"));
                   textView.setText(jsonPart.getString("url"));
                   // Log.i("Description",jsonPart.getString("description"));
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

