package com.example.bulbul.contryprofile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        imageView= (ImageView) findViewById(R.id.imageViewId);
        textView= (TextView) findViewById(R.id.textViewId);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){

            String countryNames=bundle.getString("name");
            showDetails(countryNames);

        }
    }

    void showDetails(String countryNames){
        if(countryNames.equals("bangladesh")){
            imageView.setImageResource(R.drawable.bd_profile);
            textView.setText(R.string.bd_profile_text);
        }
        if(countryNames.equals("india")){
            imageView.setImageResource(R.drawable.ind_profile);
            textView.setText(R.string.bd_profile_text);
        }
        if(countryNames.equals("pakistan")){
            imageView.setImageResource(R.drawable.pak_profile);
            textView.setText(R.string.bd_profile_text);
        }
    }
}
