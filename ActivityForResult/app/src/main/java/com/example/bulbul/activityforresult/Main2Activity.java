package com.example.bulbul.activityforresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private Button button;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        button = (Button) findViewById(R.id.button2Id);
        editText= (EditText) findViewById(R.id.editTextId);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String value = editText.getText().toString();
                    Intent intent=new Intent(Main2Activity.this,MainActivity.class);
                    intent.putExtra("key",value);
                    setResult(1,intent);
                    finish();

            }
        });
    }
}
