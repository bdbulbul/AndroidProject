package com.example.bulbul.buttonproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button loginButton,logoutButton;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton=(Button) findViewById(R.id.loginId);
        logoutButton= (Button) findViewById(R.id.logoutId);
        textView=(TextView)findViewById(R.id.textId);




    }
    public void showMessage(View v){
        if(v.getId()==R.id.loginId){
            Toast toast=Toast.makeText(MainActivity.this,"Login Button is CLicked",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP,0,0);
            toast.show();
        }else if(v.getId()==R.id.logoutId){
            Toast toast=Toast.makeText(MainActivity.this,"Logout Button is CLicked",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM,0,0);
            toast.show();
        }
    }



}
