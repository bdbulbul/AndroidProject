package com.example.bulbul.eidappstwodemoanotheractivity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EidBinodon extends AppCompatActivity {
    private Button eidSms,eidGojol,eidPicnic;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eid_binodon);

        //Find ID

        eidSms= (Button) findViewById(R.id.smsId);
        eidGojol= (Button) findViewById(R.id.eidgojolId);
        eidPicnic= (Button) findViewById(R.id.picnicSpotId);

        //Onclick Listener

        eidSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidBinodon.this);
                alertDialogBuilder.setMessage(R.string.eid_binodon_sms_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidGojol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidBinodon.this);
                alertDialogBuilder.setMessage(R.string.eid_binodon_gojol_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidPicnic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidBinodon.this);
                alertDialogBuilder.setMessage(R.string.eid_binodon_center_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        }
}
