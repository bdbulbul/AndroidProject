package com.example.bulbul.eidappstwodemoanotheractivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button eidUlFitar,eidulAdha,eidBinodon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        eidUlFitar= (Button) findViewById(R.id.eidulFitrId);
        eidulAdha= (Button) findViewById(R.id.eidulAdhaId);
        eidBinodon= (Button) findViewById(R.id.button3);

        eidUlFitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(MainActivity.this,EidUlFitar.class);
                startActivity(intent1);
            }
        });

        eidulAdha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(MainActivity.this,EidUlAdha.class);
                startActivity(intent2);
            }
        });

        eidBinodon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3=new Intent(MainActivity.this,EidBinodon.class);
                startActivity(intent3);
            }
        });

    }
}
