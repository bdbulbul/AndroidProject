package com.example.bulbul.eidappstwodemoanotheractivity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EidUlAdha extends AppCompatActivity {
    private Button eidulAdhaMeaningText,eidulAdhaStartText,eidulAdhaSalatText,eidulAdhaBeforeSalatText,eidulAdhaKurbani;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eid_ul_adha);

        //Find Id

        eidulAdhaMeaningText= (Button) findViewById(R.id.eidulAdhaId);
        eidulAdhaStartText= (Button) findViewById(R.id.eidul_Adha_start_Id);
        eidulAdhaSalatText= (Button) findViewById(R.id.eidul_Adha_Salat_Id);
        eidulAdhaBeforeSalatText= (Button) findViewById(R.id.eidul_Adha_Before_Salat_Id);
        eidulAdhaKurbani= (Button) findViewById(R.id.eidul_Adha_Kurbani_Id);


        //Onclick Button

        eidulAdhaMeaningText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlAdha.this);
                alertDialogBuilder.setMessage(R.string.eidul_adha_meaning_text);
                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidulAdhaStartText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlAdha.this);
                alertDialogBuilder.setMessage(R.string.eidul_adha_start_text);
                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidulAdhaSalatText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlAdha.this);
                alertDialogBuilder.setMessage(R.string.eidul_adha_salat_text);
                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidulAdhaBeforeSalatText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlAdha.this);
                alertDialogBuilder.setMessage(R.string.eidul_adha_before_salat_text);
                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        eidulAdhaKurbani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlAdha.this);
                alertDialogBuilder.setMessage(R.string.eidul_adha_kurbani_text);
                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });



    }
}
