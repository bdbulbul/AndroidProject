package com.example.bulbul.eidappstwodemoanotheractivity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EidUlFitar extends AppCompatActivity {
    private Button eidulFitrMeaningText, eidulFitrDateText, rulesOfSalatText, beforeSalatText;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eid_ul_fitar);

        //Find ID
        eidulFitrMeaningText = (Button) findViewById(R.id.eidul_fitr_meaningId);
        eidulFitrDateText = (Button) findViewById(R.id.eidulFitr_start_Id);
        rulesOfSalatText = (Button) findViewById(R.id.eidulFitr_salat_Id);
        beforeSalatText = (Button) findViewById(R.id.eidulFitr_beforeSalat_Id);

        //Button Eid Ul Fitar

        eidulFitrMeaningText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlFitar.this);
                alertDialogBuilder.setMessage(R.string.eidul_fitr_meaning_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });



        eidulFitrDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlFitar.this);
                alertDialogBuilder.setMessage(R.string.eidul_fitr_date_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });





        rulesOfSalatText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlFitar.this);
                alertDialogBuilder.setMessage(R.string.eidul_fitr_salat_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });




        beforeSalatText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(EidUlFitar.this);
                alertDialogBuilder.setMessage(R.string.eidul_fitr_before_salat_text);


                alertDialogBuilder.setNegativeButton("Ok!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });







    }
}



