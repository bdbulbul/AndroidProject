package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BanglaNewsPapers extends AppCompatActivity {

    private Button prothomAlo,amarDesh,kalerKontho,noyaDigonto,jugantor,destiny,ittefak,manobjomin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bangla_news_papers);

        prothomAlo= (Button) findViewById(R.id.button6);
        amarDesh= (Button) findViewById(R.id.button7);
        kalerKontho= (Button) findViewById(R.id.button8);
        noyaDigonto= (Button) findViewById(R.id.button9);
        jugantor= (Button) findViewById(R.id.button10);
        destiny= (Button) findViewById(R.id.button11);
        ittefak= (Button) findViewById(R.id.button12);
        manobjomin= (Button) findViewById(R.id.button13);

        prothomAlo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prothomAlo=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                prothomAlo.putExtra("name","prothomAlo");
                startActivity(prothomAlo);
            }
        });

        amarDesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent amarDesh=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                amarDesh.putExtra("name","amardesh");
                startActivity(amarDesh);
            }
        });


        kalerKontho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kalerKontho=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                kalerKontho.putExtra("name","kalerkontho");
                startActivity(kalerKontho);
            }
        });

        noyaDigonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noyaDigonto=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                noyaDigonto.putExtra("name","noyadigonto");
                startActivity(noyaDigonto);
            }
        });


        jugantor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent jugantor=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                jugantor.putExtra("name","jugantor");
                startActivity(jugantor);
            }
        });


        destiny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent destiny=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                destiny.putExtra("name","destiny");
                startActivity(destiny);
            }
        });

        ittefak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ittefak=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                ittefak.putExtra("name","ittefak");
                startActivity(ittefak);
            }
        });

        manobjomin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent manobjomin=new Intent(BanglaNewsPapers.this,WebViewShow.class);
                manobjomin.putExtra("name","ittefak");
                startActivity(manobjomin);
            }
        });
    }
}
