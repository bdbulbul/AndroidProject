package com.example.bulbul.newspaperapp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewShow extends AppCompatActivity {
    private WebView showWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_show);

        showWebView= (WebView) findViewById(R.id.protholAloWebViewId);



        Bundle bundle=getIntent().getExtras();
        if(bundle !=null){
            String newsPaperName=bundle.getString("name");
            showWebViewMethod(newsPaperName);
        }
    }




    void showWebViewMethod(String newsPaperName){

        //------------------Bangla NewsPaper url load start---------------------------------

        if(newsPaperName.equals("amardesh")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.amar-desh24.com/bangla/");
        }
        if(newsPaperName.equals("prothomAlo")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.prothom-alo.com");
        }

        if(newsPaperName.equals("kalerkontho")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.kalerkantho.com");
        }

        if(newsPaperName.equals("noyadigonto")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.dailynayadiganta.com");
        }

        if(newsPaperName.equals("jugantor")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.jugantor.com");
        }

        if(newsPaperName.equals("destiny")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.dainik-destiny.com");
        }

        if(newsPaperName.equals("ittefak")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.ittefaq.com.bd");
        }

        if(newsPaperName.equals("manobjomin")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.mzamin.com");
        }

        //----------------------English NewsPaper Start---------------------------------------------

        if(newsPaperName.equals("dailystar")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.thedailystar.net");
        }

        if(newsPaperName.equals("dailysun")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.daily-sun.com");
        }

        if(newsPaperName.equals("dailyobserver")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.observerbd.com");
        }

        if(newsPaperName.equals("newstoday")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.newstoday.com.bd");
        }

        if(newsPaperName.equals("theindependent")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.theindependentbd.com");
        }

        if(newsPaperName.equals("bbcnews")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.bbc.com/news");
        }

        if(newsPaperName.equals("energybangla")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://energybangla.com.bd");
        }

        if(newsPaperName.equals("report24")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://bangla.thereport24.com");
        }

        //-----------------------------Sports news--------------------------------------


        if(newsPaperName.equals("cricbuzz")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.cricbuzz.com");
        }

        if(newsPaperName.equals("espnCric")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.espncricinfo.com");
        }

        //--------------------------Online News----------------------------------------------------

        if(newsPaperName.equals("sheershanews")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("https://www.sheershanews24.com");
        }


        if(newsPaperName.equals("bdnews24")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("https://bdnews24.com");
        }

        if(newsPaperName.equals("banglamail")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://bdtoday24.com");
        }

        if(newsPaperName.equals("banglanews24")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.banglanews24.com");
        }

        if(newsPaperName.equals("banglatribune")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.banglatribune.com");
        }

        if(newsPaperName.equals("priyo")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.priyo.com");
        }
        //--------------------------------Online Tv----------------------------------------

        if(newsPaperName.equals("jagobd")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.jagobd.com");
        }

        if(newsPaperName.equals("bioscopelive")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.bioscopelive.com");
        }

        //------------------------FM-------------------------------------------

        if(newsPaperName.equals("radioforti")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://radiofoorti.fm");
        }

        if(newsPaperName.equals("radiotoday")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://radiotodaybd.fm");
        }

        if(newsPaperName.equals("abcradio")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("https://www.abcradiobd.fm");
        }

        if(newsPaperName.equals("dhakafm")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.dhakafm904.com");
        }

        if(newsPaperName.equals("radioshadin")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://radioshadhin.fm");
        }
        if(newsPaperName.equals("radiopeople")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://peoplesradio.fm");
        }

        if(newsPaperName.equals("allradio")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.loadUrl("http://www.radio.net.bd");
        }
    }

    @Override
    public void onBackPressed() {
        if(showWebView.canGoBack()){
            showWebView.goBack();
        }else {
            super.onBackPressed();
        }
    }
}
