package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OnlineTv extends AppCompatActivity {

    private Button jagoBd,bioscopeLive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_tv);

        jagoBd= (Button) findViewById(R.id.button28);
        bioscopeLive= (Button) findViewById(R.id.button29);


        jagoBd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent jagoBd=new Intent(OnlineTv.this,WebViewShow.class);
                jagoBd.putExtra("name","jagobd");
                startActivity(jagoBd);
            }
        });

        bioscopeLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bioscopeLive=new Intent(OnlineTv.this,WebViewShow.class);
                bioscopeLive.putExtra("name","bioscopelive");
                startActivity(bioscopeLive);
            }
        });
    }
}
