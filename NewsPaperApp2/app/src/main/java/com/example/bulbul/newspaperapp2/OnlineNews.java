package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OnlineNews extends AppCompatActivity {

    private Button sheershaNews,bdNews24,banglaMail,banglaNews24,banglaTribune,priyo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_news);

        sheershaNews= (Button) findViewById(R.id.button22);
        bdNews24= (Button) findViewById(R.id.button23);
        banglaMail= (Button) findViewById(R.id.button24);
        banglaNews24= (Button) findViewById(R.id.button25);
        banglaTribune= (Button) findViewById(R.id.button26);
        priyo= (Button) findViewById(R.id.button27);


        sheershaNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sheershaNews=new Intent(OnlineNews.this,WebViewShow.class);
                sheershaNews.putExtra("name","sheershanews");
                startActivity(sheershaNews);
            }
        });

        bdNews24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bdNews24=new Intent(OnlineNews.this,WebViewShow.class);
                bdNews24.putExtra("name","bdnews24");
                startActivity(bdNews24);
            }
        });


        banglaMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent banglaMail=new Intent(OnlineNews.this,WebViewShow.class);
                banglaMail.putExtra("name","banglamail");
                startActivity(banglaMail);
            }
        });

        banglaNews24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent banglaNews24=new Intent(OnlineNews.this,WebViewShow.class);
                banglaNews24.putExtra("name","banglanews24");
                startActivity(banglaNews24);
            }
        });

        banglaTribune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent banglaTribune=new Intent(OnlineNews.this,WebViewShow.class);
                banglaTribune.putExtra("name","banglatribune");
                startActivity(banglaTribune);
            }
        });

        priyo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent priyo=new Intent(OnlineNews.this,WebViewShow.class);
                priyo.putExtra("name","priyo");
                startActivity(priyo);
            }
        });
    }
}
