package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FMRadio extends AppCompatActivity {

    private Button radioFoorti,radioToday,abcRadio,dhakaFm,radioShadin,radioPeople,allRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fmradio);

        radioFoorti= (Button) findViewById(R.id.button33);
        radioToday= (Button) findViewById(R.id.button34);
        abcRadio= (Button) findViewById(R.id.button35);
        dhakaFm= (Button) findViewById(R.id.button36);
        radioShadin= (Button) findViewById(R.id.button37);
        radioPeople= (Button) findViewById(R.id.button38);
        allRadio= (Button) findViewById(R.id.button39);




        radioFoorti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent radioFoorti=new Intent(FMRadio.this,WebViewShow.class);
                radioFoorti.putExtra("name","radiofoorti");
                startActivity(radioFoorti);
            }
        });


        radioToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent radioToday=new Intent(FMRadio.this,WebViewShow.class);
                radioToday.putExtra("name","radiotoday");
                startActivity(radioToday);
            }
        });


        abcRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abcRadio=new Intent(FMRadio.this,WebViewShow.class);
                abcRadio.putExtra("name","abcradio");
                startActivity(abcRadio);
            }
        });


        dhakaFm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dhakaFm=new Intent(FMRadio.this,WebViewShow.class);
                dhakaFm.putExtra("name","dhakafm");
                startActivity(dhakaFm);
            }
        });

        radioShadin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent radioShadin=new Intent(FMRadio.this,WebViewShow.class);
                radioShadin.putExtra("name","radioshadin");
                startActivity(radioShadin);
            }
        });

        radioPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent radioPeople=new Intent(FMRadio.this,WebViewShow.class);
                radioPeople.putExtra("name","radiopeople");
                startActivity(radioPeople);
            }
        });

        allRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent allRadio=new Intent(FMRadio.this,WebViewShow.class);
                allRadio.putExtra("name","allradio");
                startActivity(allRadio);
            }
        });


    }
}
