package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button banglaNewsButton,englishNewsButton,sportsNewsButton,onlineNewsButton,onlineTv,Fm;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        banglaNewsButton= (Button) findViewById(R.id.button);
        englishNewsButton= (Button) findViewById(R.id.button2);
        sportsNewsButton= (Button) findViewById(R.id.button3);
        onlineNewsButton= (Button) findViewById(R.id.button4);
        onlineTv= (Button) findViewById(R.id.button5);
        Fm= (Button) findViewById(R.id.button32);


        banglaNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent banglaNews=new Intent(MainActivity.this,BanglaNewsPapers.class);
                startActivity(banglaNews);
            }
        });

        englishNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent englishNews=new Intent(MainActivity.this,EnglishNewsPapers.class);
                startActivity(englishNews);
            }
        });

        sportsNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sports = new Intent(MainActivity.this,SportsNews.class);
                startActivity(sports);
            }
        });

        onlineNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent onlineNews = new Intent(MainActivity.this,OnlineNews.class);
                startActivity(onlineNews);
            }
        });

        onlineTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent onlineTv = new Intent(MainActivity.this,OnlineTv.class);
                startActivity(onlineTv);
            }
        });

        Fm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Fm = new Intent(MainActivity.this,FMRadio.class);
                startActivity(Fm);
            }
        });



    }


}