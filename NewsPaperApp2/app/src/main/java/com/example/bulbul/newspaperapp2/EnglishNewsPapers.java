package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EnglishNewsPapers extends AppCompatActivity {
    private Button dailyStar,dailySun,dailyObserver,newsToday,theIndependent,bbcNews,energyBangla,report24;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english_news_papers);

        dailyStar= (Button) findViewById(R.id.button14);
        dailySun= (Button) findViewById(R.id.button15);
        dailyObserver= (Button) findViewById(R.id.button16);
        newsToday= (Button) findViewById(R.id.button17);
        theIndependent= (Button) findViewById(R.id.button18);
        bbcNews= (Button) findViewById(R.id.button19);
        energyBangla= (Button) findViewById(R.id.button30);
        report24= (Button) findViewById(R.id.button31);


        dailyStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dailyStar=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                dailyStar.putExtra("name","dailystar");
                startActivity(dailyStar);
            }
        });


        dailySun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dailySun=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                dailySun.putExtra("name","dailysun");
                startActivity(dailySun);
            }
        });

        dailyObserver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dailyObserver=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                dailyObserver.putExtra("name","dailyobserver");
                startActivity(dailyObserver);
            }
        });

        newsToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newsToday=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                newsToday.putExtra("name","newstoday");
                startActivity(newsToday);
            }
        });


        theIndependent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent theIndependent=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                theIndependent.putExtra("name","theindependent");
                startActivity(theIndependent);
            }
        });

        bbcNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bbcNews=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                bbcNews.putExtra("name","bbcnews");
                startActivity(bbcNews);
            }
        });

        energyBangla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent energyBangla=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                energyBangla.putExtra("name","energybangla");
                startActivity(energyBangla);
            }
        });

        energyBangla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent energyBangla=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                energyBangla.putExtra("name","energybangla");
                startActivity(energyBangla);
            }
        });

        report24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent report24=new Intent(EnglishNewsPapers.this,WebViewShow.class);
                report24.putExtra("name","report24");
                startActivity(report24);
            }
        });







    }
}
