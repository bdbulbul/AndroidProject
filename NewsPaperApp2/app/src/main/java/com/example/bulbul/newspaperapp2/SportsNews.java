package com.example.bulbul.newspaperapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SportsNews extends AppCompatActivity {

    private Button cricBuzz,espnCric;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_news);

        cricBuzz= (Button) findViewById(R.id.button20);
        espnCric= (Button) findViewById(R.id.button21);


        cricBuzz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cricBuzz=new Intent(SportsNews.this,WebViewShow.class);
                cricBuzz.putExtra("name","cricbuzz");
                startActivity(cricBuzz);
            }
        });

        espnCric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent espnCric=new Intent(SportsNews.this,WebViewShow.class);
                espnCric.putExtra("name","espnCric");
                startActivity(espnCric);
            }
        });
    }
}
