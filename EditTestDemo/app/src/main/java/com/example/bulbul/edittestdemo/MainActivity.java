package com.example.bulbul.edittestdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editText1,editText2;
    private Button addButton,subButton;
    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1= (EditText) findViewById(R.id.editText1Id);
        editText2= (EditText) findViewById(R.id.editText2Id);

        addButton= (Button) findViewById(R.id.addButtonId);
        subButton= (Button) findViewById(R.id.subButtonId);

        resultView= (TextView) findViewById(R.id.textViewId);

        addButton.setOnClickListener(this);
        subButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        try{
            String number1= editText1.getText().toString();
            String number2= editText2.getText().toString();
            //Converting
            double num1=Double.parseDouble(number1);
            double num2=Double.parseDouble(number2);

            if(v.getId()==R.id.addButtonId){
                double sum=num1+num2;
                resultView.setText("Result is "+sum);
            }
            if(v.getId()==R.id.subButtonId){
                double sub=num1-num2;
                resultView.setText("Result is "+sub);
            }

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Please Enter Number", Toast.LENGTH_SHORT).show();
        }

    }
}
