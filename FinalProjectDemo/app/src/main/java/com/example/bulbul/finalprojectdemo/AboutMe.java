package com.example.bulbul.finalprojectdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutMe extends AppCompatActivity {
    private ImageView myYoutube,myFacebook,myTwitter,myGithub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);

        myYoutube= (ImageView) findViewById(R.id.myYtv);
        myFacebook= (ImageView) findViewById(R.id.myFb);
        myTwitter= (ImageView) findViewById(R.id.myTwt);
        myGithub= (ImageView) findViewById(R.id.myGit);

        //Catch me social media
        myYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent youtube = new Intent(AboutMe.this,ShowFullNews.class);
                youtube.putExtra("name","youtube");
                startActivity(youtube);
            }
        });
        myFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebook = new Intent(AboutMe.this,ShowFullNews.class);
                facebook.putExtra("name","facebook");
                startActivity(facebook);
            }
        });

        myTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent twitter = new Intent(AboutMe.this,ShowFullNews.class);
                twitter.putExtra("name","twitter");
                startActivity(twitter);
            }
        });
        myGithub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent github = new Intent(AboutMe.this,ShowFullNews.class);
                github.putExtra("name","github");
                startActivity(github);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_layout,menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.englishNewsId){
            Intent i = new Intent(AboutMe.this,Display.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.banglaNewsId){
            Intent i = new Intent(AboutMe.this,BanglaNews.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.sportsNewsId){
            Intent i = new Intent(AboutMe.this,SportsNews.class);
            startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }
}
