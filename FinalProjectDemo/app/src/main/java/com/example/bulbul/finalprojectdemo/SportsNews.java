package com.example.bulbul.finalprojectdemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SportsNews extends AppCompatActivity {
    private AdView mAdView;
    private InterstitialAd interstitialAd;
    TextView newsView5,newsView6,newsView7,newsView8;
    ImageView youTubeThree,youTubeFour;
    String url5="",url6="",url7="",url8="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_news);


        newsView5= (TextView) findViewById(R.id.currentNews5);
        newsView6= (TextView) findViewById(R.id.currentNews6);
        newsView7= (TextView) findViewById(R.id.currentNews7);
        newsView8= (TextView) findViewById(R.id.currentNews8);

        //Youtube Player View
        youTubeThree= (ImageView) findViewById(R.id.ytvLinkThree);
        youTubeFour= (ImageView) findViewById(R.id.ytvLinkFour);

        //admob banner add show
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        mAdView = (AdView) findViewById(R.id.adViewSport);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        setupInterstialAd();


        DownloadTask5 task5 = new DownloadTask5();
        DownloadTask6 task6 = new DownloadTask6();
        DownloadTask7 task7 = new DownloadTask7();
        DownloadTask8 task8 = new DownloadTask8();



        task5.execute("https://newsapi.org/v1/articles?source=bbc-sport&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task6.execute("https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task7.execute("https://newsapi.org/v1/articles?source=espn-cric-info&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task8.execute("https://newsapi.org/v1/articles?source=football-italia&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");

        //------------------------BBC Sports Apl Title Onclick---------------

        newsView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bbcSports = new Intent(SportsNews.this,ShowFullNews.class);
                bbcSports.putExtra("name","bbcSports");
                bbcSports.putExtra("url",url5);
                startActivity(bbcSports);
            }
        });
        //------------------------Espn  Apl Title Onclick---------------

        newsView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent espn = new Intent(SportsNews.this,ShowFullNews.class);
                espn.putExtra("name","espn");
                espn.putExtra("url",url6);
                startActivity(espn);
            }
        });

        //---------------------ESPN CRIC AU API title Onclick---------------------------------

        newsView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent espnCric = new Intent(SportsNews.this,ShowFullNews.class);
                espnCric.putExtra("name","espnCric");
                espnCric.putExtra("url",url7);
                startActivity(espnCric);
            }
        });

        //---------------------Football Italian  News----------------------------

        newsView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent footballItalia = new Intent(SportsNews.this,ShowFullNews.class);
                footballItalia.putExtra("name","footballItalia");
                footballItalia.putExtra("url",url8);
                startActivity(footballItalia);
            }
        });

        //.................Watching Youtube Player...........................

        youTubeThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent three=new Intent(SportsNews.this,YoutubePlayerViewThree.class);
                startActivity(three);
            }
        });

        youTubeFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent four=new Intent(SportsNews.this,YoutubePlayerViewTwo.class);
                startActivity(four);
            }
        });



    }

    //Interstetial admob method

    private void setupInterstialAd() {
        interstitialAd = new InterstitialAd(SportsNews.this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.interstetial_add_unit_id));
        AdRequest adRequestFull = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequestFull);
        interstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_layout,menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.englishNewsId){
            Intent i = new Intent(SportsNews.this,Display.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.banglaNewsId){
            Intent i = new Intent(SportsNews.this,BanglaNews.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.sportsNewsId){
            Intent i = new Intent(SportsNews.this,SportsNews.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.aboutMe){
            Intent i = new Intent(SportsNews.this,AboutMe.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }


    // Json Parsing start code ---------------BBC Sports HeadLines-------------

    public class DownloadTask5 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    // Log.i("Main",jsonPart.getString("main"));
                    // textView.setText(jsonPart.getString("title"));
                    newsView5.setText(jsonPart.getString("title"));
                    url5=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //-----------------ESPN News---------------------------

    public class DownloadTask6 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView6.setText(jsonPart.getString("title"));
                    url6=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //------------------------------ESPN CRIC news AU---------------------------------------------

    public class DownloadTask7 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView7.setText(jsonPart.getString("title"));
                    url7=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //-----------------Footbal Itallian News--------------------------
    public class DownloadTask8 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView8.setText(jsonPart.getString("title"));
                    url8=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}
