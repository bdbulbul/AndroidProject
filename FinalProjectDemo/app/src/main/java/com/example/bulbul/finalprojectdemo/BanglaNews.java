package com.example.bulbul.finalprojectdemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BanglaNews extends AppCompatActivity {
    private AdView mAdView;
    private InterstitialAd interstitialAd;

    TextView newsView9,newsView10,newsView11,newsView12;
    ImageView youTubeFive,youTubeSix;
    String url9="",url10="",url11="",url12="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bangla_news);


        newsView9= (TextView) findViewById(R.id.currentNews9);
        newsView10= (TextView) findViewById(R.id.currentNews10);
        newsView11= (TextView) findViewById(R.id.currentNews11);
        newsView12= (TextView) findViewById(R.id.currentNews12);

        //Youtube Player View
        youTubeFive= (ImageView) findViewById(R.id.ytvLinkFive);
        youTubeSix= (ImageView) findViewById(R.id.ytvLinkSix);

        //admob banner add show
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        mAdView = (AdView) findViewById(R.id.adViewBangla);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        //Interstetial add
        setupInterstialAd();


        DownloadTask9 task9 = new DownloadTask9();
        DownloadTask10 task10 = new DownloadTask10();
        DownloadTask11 task11 = new DownloadTask11();
        DownloadTask12 task12 = new DownloadTask12();



        task9.execute("https://newsapi.org/v1/articles?source=national-geographic&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task10.execute("https://newsapi.org/v1/articles?source=independent&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task11.execute("https://newsapi.org/v1/articles?source=mtv-news&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task12.execute("https://newsapi.org/v1/articles?source=hacker-news&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");

        //------------------------BBC Sports Apl Title Onclick---------------

        newsView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nationalGeo = new Intent(BanglaNews.this,ShowFullNews.class);
                nationalGeo.putExtra("name","nationalGeo");
                nationalGeo.putExtra("url",url9);
                startActivity(nationalGeo);
            }
        });
        //------------------------Espn  Apl Title Onclick---------------

        newsView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent independent = new Intent(BanglaNews.this,ShowFullNews.class);
                independent.putExtra("name","independent");
                independent.putExtra("url",url10);
                startActivity(independent);
            }
        });

        //---------------------ESPN CRIC AU API title Onclick---------------------------------

        newsView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mtvNews = new Intent(BanglaNews.this,ShowFullNews.class);
                mtvNews.putExtra("name","mtvNews");
                mtvNews.putExtra("url",url11);
                startActivity(mtvNews);
            }
        });

        //---------------------Football Italian  News----------------------------

        newsView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hackerNews = new Intent(BanglaNews.this,ShowFullNews.class);
                hackerNews.putExtra("name","hackerNews");
                hackerNews.putExtra("url",url12);
                startActivity(hackerNews);
            }
        });

        //.................Watching Youtube Player...........................

        youTubeFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent five=new Intent(BanglaNews.this,YoutubePlayerViewFour.class);
                startActivity(five);
            }
        });
        youTubeSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent six=new Intent(BanglaNews.this,YoutubePlayerViewTwo.class);
                startActivity(six);
            }
        });



    }

    private void setupInterstialAd() {
        interstitialAd = new InterstitialAd(BanglaNews.this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.interstetial_add_unit_id));
        AdRequest adRequestFull = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequestFull);
        interstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_layout,menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.englishNewsId){
            Intent i = new Intent(BanglaNews.this,Display.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.banglaNewsId){


        }else if(item.getItemId()==R.id.sportsNewsId){
            Intent i = new Intent(BanglaNews.this,SportsNews.class);
            startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }


    // Json Parsing start code ---------------BBC Sports HeadLines-------------

    public class DownloadTask9 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    // Log.i("Main",jsonPart.getString("main"));
                    // textView.setText(jsonPart.getString("title"));
                    newsView9.setText(jsonPart.getString("title"));
                    url9=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //-----------------ESPN News---------------------------

    public class DownloadTask10 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView10.setText(jsonPart.getString("title"));
                    url10=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //------------------------------ESPN CRIC news AU---------------------------------------------

    public class DownloadTask11 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView11.setText(jsonPart.getString("title"));
                    url11=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //-----------------Footbal Itallian News--------------------------
    public class DownloadTask12 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView12.setText(jsonPart.getString("title"));
                    url12=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}

