package com.example.bulbul.finalprojectdemo;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Display extends AppCompatActivity {
   // private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    //private AdView myAdView;
    private InterstitialAd interstitialAd;

    TextView newsView,newsView2,newsView3,newsView4;
    ImageView youTubeOne,youTubeTwo;
    String url="",url2="",url3="",url4="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);


        newsView= (TextView) findViewById(R.id.currentNews);
        newsView2= (TextView) findViewById(R.id.currentNews2);
        newsView3= (TextView) findViewById(R.id.currentNews3);
        newsView4= (TextView) findViewById(R.id.currentNews4);

        //Youtube Player View
        youTubeOne= (ImageView) findViewById(R.id.ytvLinkOne);
        youTubeTwo= (ImageView) findViewById(R.id.ytvLinkTwo);


        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

      /*  mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build()); */

        //admob
       /* myAdview= (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        myAdview.loadAd(adRequest);
        */

        setupInterstialAd();
        //setupBannerAd();



        DownloadTask task=new DownloadTask();
        DownloadTask2 task2=new DownloadTask2();
        DownloadTask3 task3=new DownloadTask3();
        DownloadTask4 task4=new DownloadTask4();


        task.execute("https://newsapi.org/v1/articles?source=al-jazeera-english&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task2.execute("https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task3.execute("https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");
        task4.execute("https://newsapi.org/v1/articles?source=ars-technica&sortBy=top&apiKey=50867244a3f04d69b730457261999c25");

        //------------------------Aj Jajira Apl Title Onclick---------------

        newsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alJajeera = new Intent(Display.this,ShowFullNews.class);
                alJajeera.putExtra("name","alJajeera");
                alJajeera.putExtra("url",url);
                startActivity(alJajeera);
            }
        });
        //------------------------BBC News Apl Title Onclick---------------

        newsView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bbcNews = new Intent(Display.this,ShowFullNews.class);
                bbcNews.putExtra("name","bbcNews");
                bbcNews.putExtra("url",url2);
                startActivity(bbcNews);
            }
        });

        //---------------------ABC news AU API title Onclick---------------------------------

        newsView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abcNews = new Intent(Display.this,ShowFullNews.class);
                abcNews.putExtra("name","abcNews");
                abcNews.putExtra("url",url3);
                startActivity(abcNews);
            }
        });

        //---------------------Ars Technica News----------------------------

        newsView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent arsNews = new Intent(Display.this,ShowFullNews.class);
                arsNews.putExtra("name","arsNews");
                arsNews.putExtra("url",url4);
                startActivity(arsNews);
            }
        });

        //.................Watching Youtube Player...........................

        youTubeOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent one=new Intent(Display.this,YoutubePlayerView.class);
                startActivity(one);
            }
        });
        youTubeTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent two = new Intent(Display.this,YoutubePlayerViewTwo.class);
                startActivity(two);
            }
        });



    }

 /*   private void setupBannerAd() {
        myAdView= (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        myAdView.loadAd(adRequest);
        AdView myAdView = new AdView(this);
        myAdView.setAdSize(com.google.android.gms.ads.AdSize.SMART_BANNER);
    } */

  private void setupInterstialAd() {
        interstitialAd = new InterstitialAd(Display.this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.interstetial_add_unit_id));
        AdRequest adRequestFull = new AdRequest.Builder().build();
        interstitialAd.loadAd(adRequestFull);
        interstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }

        });
    }



    //admob Code
  /*  @Override
    public void onPause(){
        if(myAdview != null){
            myAdview.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume(){
        if(myAdview != null){
            myAdview.resume();
        }
        super.onResume();
    }

    @Override
    public void onDestroy(){
        if(myAdview != null){
            myAdview.destroy();
        }
        super.onDestroy();
    }
*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_layout,menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.englishNewsId){
            Intent i = new Intent(Display.this,Display.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.banglaNewsId){
            Intent i = new Intent(Display.this,BanglaNews.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.sportsNewsId){
            Intent i = new Intent(Display.this,SportsNews.class);
            startActivity(i);

        }else if(item.getItemId()==R.id.aboutMe){
            Intent i = new Intent(Display.this,AboutMe.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }


    // Json Parsing start code ---------------ALjajira News HeadLines-------------

    public class DownloadTask extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    // Log.i("Main",jsonPart.getString("main"));
                   // textView.setText(jsonPart.getString("title"));
                    newsView.setText(jsonPart.getString("title"));
                    url=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //-----------------BBC News---------------------------

    public class DownloadTask2 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView2.setText(jsonPart.getString("title"));
                    url2=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //------------------------------ABC news AU---------------------------------------------

    public class DownloadTask3 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView3.setText(jsonPart.getString("title"));
                    url3=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //-----------------Ars Technica News--------------------------
    public class DownloadTask4 extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String newsInfo=jsonObject.getString("articles");
                JSONArray array=new JSONArray(newsInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    newsView4.setText(jsonPart.getString("title"));
                    url4=jsonPart.getString("url");

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}
