package com.example.bulbul.finalprojectdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ShowFullNews extends AppCompatActivity {

    WebView showWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_full_news);

        showWebView = (WebView) findViewById(R.id.webViewId);
        //Title Show
        Bundle bundle=getIntent().getExtras();
        if(bundle !=null){
            String newsTitle=bundle.getString("name");
            String url=bundle.getString("url");
            showWebViewMethod(newsTitle,url);
            showSocialMedia(newsTitle);
        }


    }
    //Social media
    private void showSocialMedia(String newsTitle) {
        if(newsTitle.equals("youtube")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.loadUrl("https://m.youtube.com/channel/UCjbwCMgYns-wooOzZ3F9kLA");
        }

        if(newsTitle.equals("facebook")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.loadUrl("https://www.facebook.com/bd.bulbul");
        }

        if(newsTitle.equals("twitter")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.loadUrl("https://twitter.com/twt_bulbul?lang=en");
        }
        if(newsTitle.equals("github")){
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            showWebView.getSettings().setJavaScriptEnabled(true);
            showWebView.loadUrl("https://github.com/bdbulbul12");
        }
    }


    void showWebViewMethod(String newsTitle,String url) {

        //------------------English news Api load start---------------------------------

        //AlJajira

        if (newsTitle.equals("alJajeera")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if(url.equals(url)){
                showWebView.loadUrl(url);
            }
        }

        //BBC News
        if (newsTitle.equals("bbcNews")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if(url.equals(url)){
                showWebView.loadUrl(url);
            }
        }

        //ABC News

        if (newsTitle.equals("abcNews")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if(url.equals(url)){
                showWebView.loadUrl(url);
            }
        }

        //Ars Technica News

        if (newsTitle.equals("arsNews")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if(url.equals(url)){
                showWebView.loadUrl(url);
            }
        }

        // Sports News Api


        if (newsTitle.equals("bbcSports")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }

        //
        if (newsTitle.equals("espn")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }
        //

        if (newsTitle.equals("espnCric")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }
        //

        if (newsTitle.equals("footballItalia")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }

        // ----------------------Bangla News------------------------

        if (newsTitle.equals("nationalGeo")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }
        //
        if (newsTitle.equals("independent")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }
        //

        if (newsTitle.equals("mtvNews")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }
        //

        if (newsTitle.equals("hackerNews")) {
            showWebView.getSettings().getJavaScriptEnabled();
            showWebView.getSettings().setBuiltInZoomControls(true);
            showWebView.setWebViewClient(new WebViewClient());
            if (url.equals(url)) {
                showWebView.loadUrl(url);
            }
        }




    }

    @Override
    public void onBackPressed() {
        if(showWebView.canGoBack()){
            showWebView.goBack();
        }else {
            super.onBackPressed();
        }
    }
}
