package com.example.bulbul.timestableapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView timeTableListView;

    public void generateTimeTable(int timeTable){
        ArrayList<String>timeTableContent=new ArrayList<>();

        for (int i=1;i<=10;i++){
            timeTableContent.add(Integer.toString((i*timeTable)));
        }

        ArrayAdapter<String>arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,timeTableContent);
        timeTableListView.setAdapter(arrayAdapter);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SeekBar timeTablesSeekBar= (SeekBar) findViewById(R.id.seekBarId);
        timeTableListView= (ListView) findViewById(R.id.listViewId);

        timeTablesSeekBar.setMax(20);
        timeTablesSeekBar.setProgress(10);

        timeTablesSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int min=1;
                int timeTable;

                if(progress<min){
                    timeTable=min;
                }else{
                    timeTable=progress;
                }
                generateTimeTable(timeTable);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        int timeTable=10;
        generateTimeTable(10);



    }
}
