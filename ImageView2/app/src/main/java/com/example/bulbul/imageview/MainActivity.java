package com.example.bulbul.imageview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void ClickMe(View view){
        ImageButton imageButton= (ImageButton) findViewById(R.id.imageButtonId);
        imageButton.setImageResource(R.drawable.cat2);
        Toast.makeText(MainActivity.this,"Image Update Successfully",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
