package com.example.bulbul.checkboxdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CheckBox milkCheckBox,waterCheckBox,sugarCheckBox;
    private Button showButton;
    private TextView resultView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        milkCheckBox = (CheckBox) findViewById(R.id.milkCheckBoxId);
        sugarCheckBox= (CheckBox) findViewById(R.id.sugarCheckBoxId);
        waterCheckBox= (CheckBox) findViewById(R.id.waterCheckBoxId);

        showButton= (Button) findViewById(R.id.showButtonId);

        resultView= (TextView) findViewById(R.id.resultTextViewId);

        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder stringBuilder=new StringBuilder();

                if(milkCheckBox.isChecked()){
                   String value= milkCheckBox.getText().toString();
                    stringBuilder.append(value+" is Orderd"+"\n");
                }
                if(sugarCheckBox.isChecked()){
                    String value= sugarCheckBox.getText().toString();
                    stringBuilder.append(value+" is Orderd"+"\n");
                }
                if(waterCheckBox.isChecked()){
                    String value= waterCheckBox.getText().toString();
                    stringBuilder.append(value+" is Orderd");
                }
                resultView.setText(stringBuilder);
            }
        });


    }
}
