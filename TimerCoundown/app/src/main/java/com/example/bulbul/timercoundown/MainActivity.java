package com.example.bulbul.timercoundown;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new CountDownTimer(30000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

                Toast.makeText(MainActivity.this,String.valueOf(millisUntilFinished/1000),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Log.i(" Done","Timer Stopped");
            }
        }.start();


        /* final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this,1000);
                Toast.makeText(MainActivity.this,"Its RUnnable",Toast.LENGTH_SHORT).show();
            }
        };

        handler.post(runnable);
        */
    }
}
