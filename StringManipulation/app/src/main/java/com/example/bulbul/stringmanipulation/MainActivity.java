package com.example.bulbul.stringmanipulation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;

    public class ImageDownloader extends AsyncTask<String,Void,Bitmap>{
        @Override
        protected Bitmap  doInBackground(String... urls) {
            URL url;
            HttpURLConnection httpURLConnection=null;
            try {
                url=new URL(urls[0]);
                httpURLConnection=(HttpURLConnection)url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView= (ImageView) findViewById(R.id.imageView);
        textView= (TextView) findViewById(R.id.textView);


        String html ="<div class=\"carousel-poster\">\n" +
                "<img src=\"http://cdn.posh24.se/images/:full/2b023c789bcc4ad8fdd371a60dec69f6f\" alt=\"Amber Rose\"/>\n" +
                "</div>\n" +
                "";
        Pattern p = Pattern.compile("src=\"(.*?)\" ");
        Matcher m =p.matcher(html);

        while(m.find()){
            String img =m.group(1);
            ImageDownloader task = new ImageDownloader();
            Bitmap image;
            try {
                image=task.execute(img).get();
                imageView.setImageBitmap(image);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


        }


        p =Pattern.compile("alt=\"(.*?)\"");
        m =p.matcher(html);

        while(m.find()){
            textView.setText(m.group(1));

        }

    }
}
