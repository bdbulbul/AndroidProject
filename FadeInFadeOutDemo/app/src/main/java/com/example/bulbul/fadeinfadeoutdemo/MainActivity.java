package com.example.bulbul.fadeinfadeoutdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fadein(View v){
        ImageView image = (ImageView) findViewById(R.id.imageViewId);
        image.animate().alpha(0f).rotation(360).setDuration(2000);

    }

    public void fadeout(View v){
        ImageView image = (ImageView) findViewById(R.id.imageViewId);
        image.animate().alpha(1f).rotation(-360).setDuration(2000);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
