package com.example.bulbul.webcontentandgetimagefromwebsite;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import static android.R.attr.bitmap;

public class MainActivity extends AppCompatActivity {
    private TextView textView,textView2;
    private Button button;
    private ImageView imageView;




        public class ImageDownloader extends AsyncTask<String,Void,Bitmap>{
            @Override
            protected Bitmap  doInBackground(String... urls) {
               URL url;
                HttpURLConnection httpURLConnection=null;
                try {
                    url=new URL(urls[0]);
                    httpURLConnection=(HttpURLConnection)url.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }




    public class DownloadTask extends AsyncTask<String,Void,String>{
        @Override
        protected String  doInBackground(String... urls) {
			//Log.i("Url",urls[0]);
           //just ekhan theke link ta niye text View te bosaben
            return "Done";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView= (TextView) findViewById(R.id.textView);
        button= (Button) findViewById(R.id.button2);
        textView2= (TextView) findViewById(R.id.textView4);
        imageView= (ImageView) findViewById(R.id.imageView);





        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadTask task = new DownloadTask();
                String result = null;
                try {
                    result=task.execute("http://www.prothom-alo.com").get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                textView.setText(result);
                textView2.setText(R.string.website_name);

                ImageDownloader task2=new ImageDownloader();
                Bitmap image;
                try {
                    image=task2.execute("http://www.tigercricket.com.bd/wp-content/uploads/2017/04/226257.3.jpg").get();
                    imageView.setImageBitmap(image);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


            }
        });




    }
}
