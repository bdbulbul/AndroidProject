package com.example.bulbul.easylearning.CalculateCgpa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.bulbul.easylearning.R;

public class CalculateCgpa extends AppCompatActivity {

    private Button cseFirstSemster,cseSecondSemester,cseThirdSemester,cseFourthSemester,cseFifthSemester,cseSixSemester,cseSevensemester,cseEightSemester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_cgpa);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select Semester");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        cseFirstSemster = (Button) findViewById(R.id.cseFirst);
        cseSecondSemester=(Button) findViewById(R.id.cseSecond);
        cseThirdSemester=(Button) findViewById(R.id.cseThird);
        cseFourthSemester= (Button) findViewById(R.id.cseFourth);
        cseFifthSemester= (Button) findViewById(R.id.cseFifth);
        cseSixSemester= (Button) findViewById(R.id.cseSix);
        cseSevensemester= (Button) findViewById(R.id.cseSeven);
        cseEightSemester= (Button) findViewById(R.id.cseEight);

        cseFirstSemster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cseFirst = new Intent(CalculateCgpa.this,FirstSemester.class);
                startActivity(cseFirst);
            }
        });

        cseSecondSemester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cseSecond = new Intent(CalculateCgpa.this,SecondSemester.class);
                startActivity(cseSecond);
            }
        });


    }
}
