package com.example.bulbul.easylearning;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bulbul.easylearning.Admin.AddUniversity;
import com.example.bulbul.easylearning.Admin.AdminActivity;
import com.example.bulbul.easylearning.CalculateCgpa.CalculateCgpa;
import com.example.bulbul.easylearning.UniversityList;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAuth = FirebaseAuth.getInstance();



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Mail to admin", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    protected void onStart() {
        super.onStart();

        final FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0) ;
        TextView userEmail = (TextView) header.findViewById(R.id.userEmail);
        TextView userName = (TextView) header.findViewById(R.id.userName);
        final TextView verified = (TextView) header.findViewById(R.id.verify);
        String email = "";
        String name="";

        if(currentUser!=null){
            email = currentUser.getEmail();
            name = currentUser.getDisplayName();

            if(currentUser.isEmailVerified()){
                verified.setText("Verified");
                verified.setTextColor(Color.GREEN);
            }else {
                verified.setText("Email Not Verified(click to verify)");
                verified.setTextColor(Color.RED);
                verified.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(MainActivity.this,"Verification link sent to mail",Toast.LENGTH_LONG).show();
                                verified.setTextColor(Color.RED);

                            }
                        });
                    }
                });

            }
        }else{
            email = "User not logged in";
        }
        userEmail.setText(email);
        userName.setText(name);




        if(currentUser==null){
            sendToLogin();
        }
    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_university) {
            Intent universityList = new Intent(MainActivity.this,UniversityList.class);
            startActivity(universityList);
        } if (id == R.id.nav_sylabus) {

        } else if (id == R.id.nav_notice) {


        } else if (id == R.id.nav_tutorials) {

        } else if (id == R.id.nav_lab) {

        }else if (id == R.id.nav_find_cgpa) {
            Intent calculateCgpa = new Intent(MainActivity.this,CalculateCgpa.class);
            startActivity(calculateCgpa);

        }else if(id==R.id.nav_cgpa){

        }else if(id==R.id.nav_file){

        }else if(id==R.id.nav_nu){

        }else if(id==R.id.nav_chat){

        }else if(id==R.id.nav_news){

        } else if(id==R.id.nav_forum) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }else if(id==R.id.nav_logout){
            logOut();
        }
        
        

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logOut() {
        mAuth.signOut();
        sendToLogin();
    }
}
