package com.example.bulbul.easylearning.Admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bulbul.easylearning.R;
import com.example.bulbul.easylearning.UvListItem;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddUniversity extends AppCompatActivity {

    private EditText uName,uCode,uContact,uWebUrl;
    private Button addUniversity;

    DatabaseReference databaseAddUv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_university);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add University");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseAddUv= FirebaseDatabase.getInstance().getReference("university");

        uName= (EditText) findViewById(R.id.adminUvName);
        uCode= (EditText) findViewById(R.id.adminUvCode);
        uContact= (EditText) findViewById(R.id.adminUvLocation);
        uWebUrl= (EditText) findViewById(R.id.adminUvUrl);

        addUniversity= (Button) findViewById(R.id.adminAddUv);

        addUniversity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUniversity();
            }
        });
    }

    public void addUniversity(){
        String name=uName.getText().toString().trim();
        String code = uCode.getText().toString().trim();
        String contact = uContact.getText().toString().trim();
        String url=uWebUrl.getText().toString().trim();

        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(code) && !TextUtils.isEmpty(contact)){

            String id = databaseAddUv.push().getKey();
            UvListItem uvListItem = new UvListItem(name,code,contact,url);

            databaseAddUv.child(id).setValue(uvListItem);
            Toast.makeText(AddUniversity.this,"University Added Successfully",Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(AddUniversity.this,"Enter All filed",Toast.LENGTH_LONG).show();
        }
    }
}
