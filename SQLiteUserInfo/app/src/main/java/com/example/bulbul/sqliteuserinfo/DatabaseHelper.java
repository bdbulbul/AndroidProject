package com.example.bulbul.sqliteuserinfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by bulbul on 10/22/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME="STUDENT_DB";
    private static final int DATABASE_VERSION=1;
    private static final String TABLE_NAME="USER";

    private static final String COLUMN_NAME="name";
    private static final String COLUMN_ADDRESS="address";
    private static final String COLUMN_EMAIL="email";




    Context context;
    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        this.context = context;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String s="create table"+TABLE_NAME+"("+COLUMN_NAME+"Text,"+COLUMN_ADDRESS+"Text,"+COLUMN_EMAIL+"Text primary key,";
        db.execSQL(s);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //---------------adding to Data-------------------------

    void addingDataToTable(DataTemp dt){
        SQLiteDatabase sqd=this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME,dt.getName());
        cv.put(COLUMN_ADDRESS,dt.getAddress());
        cv.put(COLUMN_EMAIL,dt.getAddress());

        sqd.insert(TABLE_NAME,null,cv);
        sqd.close();
    }



}
