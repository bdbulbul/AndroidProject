package com.example.bulbul.sqliteuserinfo;

/**
 * Created by bulbul on 10/23/2017.
 */

public class DataTemp {
    String name,address,email;

    public DataTemp(String n, String a, String e){
        name=n;
        address=a;
        email=e;
    }

    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    public void setAddress(String address){
        this.address=address;
    }
    public String getAddress(){
        return address;
    }

    public void setEmail(String email){
        this.email=email;
    }
    public String getEmail(){
        return email;
    }
}
