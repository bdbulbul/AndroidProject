package com.example.bulbul.weatherappassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
private Button London,India,Usa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        London= (Button) findViewById(R.id.button);
        India= (Button) findViewById(R.id.button2);
        Usa= (Button) findViewById(R.id.button3);


        London.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent London=new Intent(MainActivity.this,London.class);
                startActivity(London);
            }
        });

        India.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent India=new Intent(MainActivity.this,India.class);
                startActivity(India);
            }
        });




    }
}
