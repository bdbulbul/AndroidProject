package com.example.bulbul.weatherappassignment;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.bulbul.weatherappassignment.R.id.textView2;

public class India extends AppCompatActivity {
    private TextView textViewMain,textViewDesc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_india);

        textViewMain= (TextView) findViewById(R.id.textView6);
        textViewDesc= (TextView) findViewById(R.id.textView7);


    DownloadTask task=new DownloadTask();
        task.execute("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1");
    }

    public class DownloadTask extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... urls) {
            String result="";
            URL url;
            HttpURLConnection urlConnection=null;
            try {
                url = new URL(urls[0]);
                urlConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=urlConnection.getInputStream();
                InputStreamReader reader=new InputStreamReader(inputStream);

                int data=reader.read();
                while(data!=-1){
                    char current= (char) data;
                    result=result+current;
                    data=reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject=new JSONObject(result);
                String weatherInfo=jsonObject.getString("weather");
                JSONArray array=new JSONArray(weatherInfo);
                for (int i=0;i<jsonObject.length();i++){
                    JSONObject jsonPart=array.getJSONObject(i);
                    textViewMain.setText(jsonPart.getString("main"));
                    textViewDesc.setText(jsonPart.getString("description"));
                    // Log.i("Description",jsonPart.getString("description"));
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
