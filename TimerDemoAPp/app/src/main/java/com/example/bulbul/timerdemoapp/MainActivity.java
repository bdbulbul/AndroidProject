package com.example.bulbul.timerdemoapp;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    SeekBar seekBar;
    TextView textView;
    CountDownTimer timer;
    Boolean timeIsActive=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (SeekBar) findViewById(R.id.seekBarId);
        textView= (TextView) findViewById(R.id.textView);
        button= (Button) findViewById(R.id.button);

        seekBar.setMax(600);//10m=600s
        seekBar.setProgress(30);//default
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                timerUpdate(progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    private void timerUpdate(int progress) {

        int minutes  = progress/60;
        int seconds=progress-minutes*60;
        textView.setText(Integer.toString(minutes)+":"+Integer.toString(seconds));
    }

    public void controlTime(View view){
        if(timeIsActive==false){

            timeIsActive=true;
            button.setText("Stop");
            seekBar.setEnabled(false);

        timer=new CountDownTimer(seekBar.getProgress()*1000, 1000) {

            public void onTick(long millisUntilFinished) {
               // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                timerUpdate((int)(millisUntilFinished/1000));

            }

            public void onFinish() {
               // mTextField.setText("done!");
                textView.setText("Done!!");
                resetTimer();
            }
        }.start();

        }else {
                resetTimer();

        }
    }



    private  void resetTimer(){
        seekBar.setEnabled(true);
        button.setText("START");
        textView.setText("0:30");
        timer.cancel();
        timeIsActive=false;
    }
}
