package com.example.bulbul.timerstopwatchdemoproject;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private TextView mon1;
    private Button startbtn, resetbtn;
    private ToggleButton togbtn;

    private boolean isPaused = false;
    private boolean isCanceled = false;
    private long Reminingtime = 0;

    Button btnStart,btnPause,btnLap,btnReset;
    TextView txtTimer;
    Handler customHandler = new Handler();
    LinearLayout container;

    long startTime=0L,timeInMilliseconds=0L,timeSwapBuff=0L,updateTime=0L;

    Runnable updateTimeThread = new Runnable() {
        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis()-startTime;
            updateTime = timeSwapBuff+timeInMilliseconds;
            int secs= (int) (updateTime/1000);
            int mins= secs/60;
            secs%=60;
            int milliseconds= (int) (updateTime%1000);
            txtTimer.setText(""+mins+":"+String.format("%2d",secs)+":"
                    +String.format("%3d",milliseconds) );
            customHandler.postDelayed(this,0);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mon1 = (TextView) findViewById(R.id.mon1);
        startbtn = (Button)findViewById(R.id.startbtn);
        resetbtn = (Button) findViewById(R.id.resetbtn);
        togbtn = (ToggleButton) findViewById(R.id.togbtn);




        resetbtn.setEnabled(false);
        togbtn.setEnabled(false);
        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startbtn.setEnabled(false);
                resetbtn.setEnabled(true);
                togbtn.setEnabled(true);

                isPaused = false;
                isCanceled = false;

                long millisInfuture = 30000;//30sec
                long countDownInterval = 1000; //1sec
                new CountDownTimer(millisInfuture, countDownInterval) {
                    @Override
                    public void onTick(long millisFinished) {
                        if(isPaused || isCanceled){
                            cancel();
                        }else {


                           long second=millisFinished/1000;
                            long minute=second/60;
                            long milisecond=minute%1000;
                            mon1.setText(minute+":"+second+":"+milisecond);


                        }

                    }

                    @Override
                    public void onFinish() {

                        mon1.setText("Times Up!");

                    }
                }.start();

            }
        });

        togbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(togbtn.isChecked()){
                    isPaused = true;
                }else {
                    isPaused = false;
                    long millisInfuture = Reminingtime;//10sec
                    long countDownInterval = 1000; //1sec
                    new CountDownTimer(millisInfuture, countDownInterval) {
                        @Override
                        public void onTick(long millisFinished) {
                            if(isPaused || isCanceled){
                                cancel();
                            }else {
                                mon1.setText(""+millisFinished/1000);
                                Reminingtime = millisFinished;
                            }

                        }

                        @Override
                        public void onFinish() {

                            mon1.setText("Times Up!");

                        }
                    }.start();
                }
            }
        });

        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCanceled = true;
                mon1.setText(String.format("%02d", 00) + ":"
                        + String.format("%02d", 00) + ":"
                        + String.format("%03d", 000));
                startbtn.setEnabled(true);
                resetbtn.setEnabled(false);

            }
        });

        btnStart = (Button) findViewById(R.id.btnStart);
        btnPause = (Button) findViewById(R.id.btnPause);
        btnLap = (Button) findViewById(R.id.btnLap);
        btnReset = (Button) findViewById(R.id.btnReset);
        txtTimer = (TextView) findViewById(R.id.timerValue);
        container =(LinearLayout) findViewById(R.id.container);


        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimeThread,0);

            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeSwapBuff+=timeInMilliseconds;
                customHandler.removeCallbacks(updateTimeThread);
            }
        });

        btnLap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View addView = inflater.inflate(R.layout.raw,null);
                TextView txtValue = (TextView) addView.findViewById(R.id.textContent);
                txtValue.setText(txtTimer.getText());
                container.addView(addView);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                txtTimer.setText(String.format("%02d", 00) + ":"
                        + String.format("%02d", 00) + ":"
                        + String.format("%03d", 000));
                startTime = SystemClock.uptimeMillis();
                timeSwapBuff = 0;

            }
        });
    }
}

