package com.example.bulbul.loginlogoutassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity {
    EditText uname,pswd;
    Button login;
    DbHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uname=(EditText)findViewById(R.id.uname);
        pswd=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=uname.getText().toString();
                String password=pswd.getText().toString();
                int id= checkUser(new User(name,password));
                if(id==-1)
                {
                    Toast.makeText(MainActivity.this,"User Does Not Exist",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent i = new Intent(MainActivity.this,Display.class);
                    i.putExtra("Username",name);
                    startActivity(i);

                    Toast.makeText(MainActivity.this,"User Exist "+name,Toast.LENGTH_SHORT).show();
                }
            }
        });
        db=new DbHandler(MainActivity.this);
//inserting dummy users
        db.addUser(new User("BULBUL", "abc"));
        db.addUser(new User("Riad", "123"));
        db.addUser(new User("Tamim", "1234"));
    }
    public int checkUser(User user)
    {
        return db.checkUser(user);
    }

}