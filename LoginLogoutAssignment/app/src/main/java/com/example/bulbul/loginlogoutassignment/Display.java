package com.example.bulbul.loginlogoutassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Display extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        String username=getIntent().getStringExtra("Username");


        TextView tv= (TextView) findViewById(R.id.TVusername);
        tv.setText(username);

    }

    public void logOutClick(View v){
        Intent i = new Intent(Display.this,MainActivity.class);
        startActivity(i);
    }



}
