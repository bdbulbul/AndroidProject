package com.example.bulbul.loveframe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    private EditText editText;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editText= (EditText) findViewById(R.id.editText);
        textView= (TextView) findViewById(R.id.textView);

        Intent in = getIntent();
        String tv1= in.getExtras().getString("location");
        textView.setText(tv1);



    }
}
